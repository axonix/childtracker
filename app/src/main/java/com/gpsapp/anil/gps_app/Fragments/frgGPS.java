package com.gpsapp.anil.gps_app.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.gpsapp.anil.gps_app.R;


public class frgGPS extends Fragment implements OnMapReadyCallback {

    MapView mapView;
    View view;
    GoogleMap gm;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.frg_gps, container, false);
        mapView = (MapView) view.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();

        mapView.onResume();
        mapView.getMapAsync(frgGPS.this); //onMapReady tetiklenmesi için
        return view;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        gm = googleMap;
        LatLng latLng = new LatLng(37.9846077, 32.5180993);
        gm.addMarker(new MarkerOptions().position(latLng).title("Ali Anıl"));
        gm.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        gm.animateCamera(CameraUpdateFactory.zoomTo(16.0f));
    }
}
