package com.gpsapp.anil.gps_app.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.gpsapp.anil.gps_app.Models.DirectMessageModel;
import com.gpsapp.anil.gps_app.R;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class DirectMessageAdapter extends RecyclerView.Adapter<DirectMessageAdapter.ViewHolder> {

    List<String> usersKeys;
    Activity activity;
    Context context;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    FirebaseAuth firebaseAuth;
    FirebaseUser firebaseUser;
    ConstraintLayout item_users_outlayout;
    String myID;
    Boolean state;

    int viewTypeInComing = 1, viewTypeOutGoing = 2;

    List<DirectMessageModel> directMessageModelList;

    public DirectMessageAdapter(List<String> usersKeys, Activity activity, Context context, List<DirectMessageModel> directMessageModelList) {
        this.usersKeys = usersKeys;
        this.activity = activity;
        this.context = context;
        this.firebaseDatabase = firebaseDatabase;
        this.databaseReference = databaseReference;
        this.directMessageModelList = directMessageModelList;

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
        myID = firebaseUser.getUid();
        state = false;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        View view;

        if (viewType == viewTypeInComing) {

            view = LayoutInflater.from(context).inflate(R.layout.item_incoming_message, viewGroup, false);
            return new ViewHolder(view);

        } else {

            view = LayoutInflater.from(context).inflate(R.layout.item_outgoing_message, viewGroup, false);
            return new ViewHolder(view);

        }


    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {


       viewHolder.messageText.setText(directMessageModelList.get(i).getMessageText());

    }

    @Override
    public int getItemViewType(int position) {
        if (directMessageModelList.get(position).getFrom().equals(myID)) {
            state = true;
            return viewTypeOutGoing;
        } else {
            state = false;
            return viewTypeInComing;
        }
    }

    @Override
    public int getItemCount() {
        return directMessageModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        TextView messageText;
        CircleImageView user_image_list;


        ViewHolder(View itemView) {
            super(itemView);
            if(state){
                messageText = itemView.findViewById(R.id.txt_outgoing_message);
            }else{
                messageText = itemView.findViewById(R.id.txt_incoming_message);
            }

        }
    }
}