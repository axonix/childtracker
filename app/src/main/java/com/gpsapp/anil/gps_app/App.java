package com.gpsapp.anil.gps_app;

import android.app.Application;
import android.content.Intent;

import com.gpsapp.anil.gps_app.Services.YourService;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        startService(new Intent(this, YourService.class));
    }
}
