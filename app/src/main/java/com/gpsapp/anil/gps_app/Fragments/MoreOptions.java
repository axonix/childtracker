package com.gpsapp.anil.gps_app.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.LogUtils;
import com.github.lzyzsd.circleprogress.CircleProgress;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.gpsapp.anil.gps_app.Activities.AppStatistic;
import com.gpsapp.anil.gps_app.Activities.BatteryStatus;
import com.gpsapp.anil.gps_app.Activities.InstalledApps;
import com.gpsapp.anil.gps_app.Activities.MainActivity;
import com.gpsapp.anil.gps_app.Activities.RunningApps;
import com.gpsapp.anil.gps_app.Activities.VoiceRecorder;
import com.gpsapp.anil.gps_app.Models.Users;
import com.gpsapp.anil.gps_app.R;
import com.gpsapp.anil.gps_app.Utils.AlertUtils;
import com.gpsapp.anil.gps_app.Utils.FirebaseHelper;
import com.gpsapp.anil.gps_app.Utils.L;
import com.gpsapp.anil.gps_app.Utils.frgUtils;

import java.util.HashMap;
import java.util.Map;

public class MoreOptions extends Fragment {

    Button more_options_voice_recorder, more_options_app_statistic, more_options_events, more_options_profile_settings, more_options_installed_apps, more_options_battery_status, more_options_running_apps, more_options_gps,
            more_options_send_alarm;
    View view;
    frgUtils frgUtils;
    String toUserID;
    AlertDialog dialog;
    CircleProgress circle_progress_battery;
    int batteryStatus;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_more_options, container, false);

        init();
        getChild();
        action();

        return view;
    }


    private void init() {

        more_options_gps = view.findViewById(R.id.more_options_gps);
        more_options_voice_recorder = view.findViewById(R.id.more_options_voice_recorder);
        more_options_send_alarm = view.findViewById(R.id.more_options_send_alarm);
        more_options_app_statistic = view.findViewById(R.id.more_options_app_statistic);
        more_options_events = view.findViewById(R.id.more_options_events);
        more_options_profile_settings = view.findViewById(R.id.more_options_profile_settings);
        more_options_installed_apps = view.findViewById(R.id.more_options_installed_apps);
        more_options_running_apps = view.findViewById(R.id.more_options_running_apps);
        more_options_battery_status = view.findViewById(R.id.more_options_battery_status);
        circle_progress_battery = view.findViewById(R.id.circle_progress_battery);


    }


    private void action() {

        more_options_gps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                frgUtils = new frgUtils(ActivityUtils.getTopActivity());
                frgUtils.change(new HomePage());
            }
        });

        more_options_voice_recorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityUtils.startActivity(ActivityUtils.getTopActivity(), VoiceRecorder.class);
            }
        });

        more_options_send_alarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Map map = new HashMap();
                map.put("status", "1");
                map.put("type", "6");

                FirebaseHelper.getDatabaseReference()
                        .child("Users")
                        .child(toUserID)
                        .child("action")
                        .setValue(map);


            }
        });

        more_options_app_statistic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityUtils.startActivity(ActivityUtils.getTopActivity(), AppStatistic.class);

            }
        });

        more_options_events.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                frgUtils = new frgUtils(ActivityUtils.getTopActivity());
                frgUtils.change(new Events());
            }
        });

        more_options_profile_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                frgUtils = new frgUtils(ActivityUtils.getTopActivity());
                frgUtils.change(new MyProfile());
            }
        });

        more_options_installed_apps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityUtils.startActivity(ActivityUtils.getTopActivity(), InstalledApps.class);
            }
        });

        more_options_running_apps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityUtils.startActivity(ActivityUtils.getTopActivity(), RunningApps.class);
            }
        });

        more_options_battery_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                batteryAlert(ActivityUtils.getTopActivity());
            }
        });

    }


    public void batteryAlert(Activity activity) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.battery_status_alert, null);


        AlertDialog.Builder alert = new AlertDialog.Builder(activity);
        alert.setView(view);
        alert.setCancelable(true);
        dialog = ((AlertDialog.Builder) alert).create();



        dialog.show();
    }

    private void getChild() {


        FirebaseHelper.getDatabaseReference()
                .child("Users")
                .child(FirebaseHelper.getFirebaseUser().getUid())
                .child("handshake")
                .child("relationship").addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot1) {
                FirebaseHelper.getDatabaseReference().child("Users").child(dataSnapshot1.getValue().toString()).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        toUserID = dataSnapshot.getKey();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }


}
