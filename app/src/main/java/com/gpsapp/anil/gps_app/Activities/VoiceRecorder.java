package com.gpsapp.anil.gps_app.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.LogUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.gpsapp.anil.gps_app.R;
import com.gpsapp.anil.gps_app.Utils.FirebaseHelper;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class VoiceRecorder extends AppCompatActivity {
    Button btn_request_record_voice,btn_play_last_recorded;
    TextView txt_request_voice_record;
    LottieAnimationView voicerec;
    AlertDialog dialog;
    String toUserID;
    int voiceRecordCount = 0;
    MediaPlayer mPlayer;
    String audioUrl = "http://www.all-birds.com/Sound/western%20bluebird.wav";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_voice_recorder);

        init();
        getChild();
        action();

    }

    public void init() {
        btn_request_record_voice = findViewById(R.id.btn_request_record_voice);
        btn_play_last_recorded = findViewById(R.id.btn_play_last_recorded);
        voicerec = findViewById(R.id.voicerec);
    }

    private void action() {

        btn_request_record_voice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert(ActivityUtils.getTopActivity());
                txt_request_voice_record.setText("İstek Gönderiliyor...");


                Map map = new HashMap();
                map.put("status", "1");
                map.put("type", "2");

                FirebaseHelper.getDatabaseReference()
                        .child("Users")
                        .child(toUserID)
                        .child("action")
                        .setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        FirebaseHelper.getDatabaseReference()
                                .child("Users")
                                .child(toUserID)
                                .child("userData")
                                .child("voiceRecord")

                                .addChildEventListener(new ChildEventListener() {
                                    @Override
                                    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                                    }

                                    @Override
                                    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                                        voiceRecordCount++;
                                        LogUtils.dTag("voiceRecordCount", voiceRecordCount + "");
                                        LogUtils.dTag("voiceRecordCount", dataSnapshot.getValue().toString() + "");
                                        audioUrl = dataSnapshot.getValue().toString();

                                        if (voiceRecordCount > 0) {
                                            dialog.dismiss();
                                        }
                                    }

                                    @Override
                                    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                                    }

                                    @Override
                                    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                    }
                });


            }
        });





        btn_play_last_recorded.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mediaPlayer = MediaPlayer.create(this, R.raw.song);
                Uri myUri = Uri.parse(audioUrl);
                try {
                    mPlayer = new MediaPlayer();
                    mPlayer.setDataSource(getApplicationContext(), myUri);
                    mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    mPlayer.prepare(); //don't use prepareAsync for mp3 playback
                    mPlayer.start();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });



    }

    public void alert(Activity activity) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.alert_layout, null);

        Button alert_no = view.findViewById(R.id.alert_hayir);
        txt_request_voice_record = view.findViewById(R.id.txt_request_voice_record);

        AlertDialog.Builder alert = new AlertDialog.Builder(activity);
        alert.setView(view);
        alert.setCancelable(false);
        dialog = ((AlertDialog.Builder) alert).create();

        alert_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        dialog.show();
    }

    private void getChild() {


        FirebaseHelper.getDatabaseReference()
                .child("Users")
                .child(FirebaseHelper.getFirebaseUser().getUid())
                .child("handshake")
                .child("relationship").addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot1) {
                FirebaseHelper.getDatabaseReference().child("Users").child(dataSnapshot1.getValue().toString()).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        toUserID = dataSnapshot.getKey();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }



    }
