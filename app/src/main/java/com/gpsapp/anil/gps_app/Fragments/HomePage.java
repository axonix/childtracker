package com.gpsapp.anil.gps_app.Fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.LogUtils;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.DoubleBounce;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.gpsapp.anil.gps_app.Activities.DirectMessage;
import com.gpsapp.anil.gps_app.Adapters.UsersAdapter;
import com.gpsapp.anil.gps_app.Models.Users;
import com.gpsapp.anil.gps_app.R;
import com.gpsapp.anil.gps_app.Utils.FirebaseHelper;
import com.gpsapp.anil.gps_app.Utils.frgUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.blankj.utilcode.util.ActivityUtils.startActivity;


public class HomePage extends Fragment implements OnMapReadyCallback {

    List<String> usersKeys;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    View view;
    RecyclerView userListRecyclerView;
    UsersAdapter usersAdapter;
    FirebaseAuth firebaseAuth;
    Button btn_gps_update2, btn_send_message_cto_child2;
    TextView selected_child_name;
    CircleImageView selected_child_pp;
    int gpsCheckerCount = 0;
    LottieAnimationView loadingMap;

    String toUserID;
    Users users;

    LatLng latLng;
    double lat;
    double lng;

    MapView mapView;
    GoogleMap gm;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home_page, container, false);
        init();
        getChild();

        mapView = (MapView) view.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();

        mapView.onResume();
        mapView.getMapAsync(HomePage.this); //onMapReady tetiklenmesi için



        //getUsers(); //list döndürür. recycler view gone durumunda


        return view;
    }

    private void getChild() {


        databaseReference.child("Users").child(firebaseAuth.getUid()).child("handshake").child("relationship").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot1) {
                LogUtils.dTag("relationdeneme", dataSnapshot1.getValue());


                databaseReference.child("Users").child(dataSnapshot1.getValue().toString()).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        users = dataSnapshot.getValue(Users.class);

                        toUserID = dataSnapshot.getKey();

                        selected_child_name.setText(users.getName());
                    }


                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        gm = googleMap;
        latLng = new LatLng(lat, lng);
        //gm.addMarker(new MarkerOptions().position(latLng).title("Ali Anıl"));
        gm.addMarker(new MarkerOptions()
                .position(latLng)
                .icon(bitmapDescriptorFromVector(getActivity(), R.drawable.ic_send))
                .title("Ali Anıl"));

        gm.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        gm.animateCamera(CameraUpdateFactory.zoomTo(15.0f));
        loadingMap.setVisibility(View.GONE);


    }



    private BitmapDescriptor bitmapDescriptorFromVector(Context context, @DrawableRes int vectorDrawableResourceId) {
        Drawable background = ContextCompat.getDrawable(context, R.drawable.locationmarker);
        background.setBounds(0, 0, background.getIntrinsicWidth(), background.getIntrinsicHeight());
      //  Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorDrawableResourceId);
       // vectorDrawable.setBounds(40, 20, vectorDrawable.getIntrinsicWidth() + 40, vectorDrawable.getIntrinsicHeight() + 20);
        Bitmap bitmap = Bitmap.createBitmap(background.getIntrinsicWidth(), background.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        background.draw(canvas);
       // vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    private void getUsers() {


        databaseReference.child("Users").child(firebaseAuth.getUid()).child("handshake").child("relationship").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot1) {
                LogUtils.dTag("relationdeneme", dataSnapshot1.getValue());
                usersKeys.add((String) dataSnapshot1.getValue());

                usersAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

       /* databaseReference.child("Users").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                //Users users = dataSnapshot.getValue(Users.class);

                LogUtils.dTag("usersList", dataSnapshot.getKey());



            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });*/
    }

    private void init() {

        ProgressBar progressBar = (ProgressBar)view.findViewById(R.id.spin_kit);
        Sprite doubleBounce = new DoubleBounce();
        progressBar.setIndeterminateDrawable(doubleBounce);


        firebaseAuth = FirebaseAuth.getInstance();
        usersKeys = new ArrayList<>();
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();

        userListRecyclerView = view.findViewById(R.id.userListRecyclerView);
        btn_gps_update2 = view.findViewById(R.id.btn_gps_update2);
        btn_send_message_cto_child2 = view.findViewById(R.id.btn_send_message_cto_child2);
        selected_child_pp = view.findViewById(R.id.selected_child_pp);
        selected_child_name = view.findViewById(R.id.selected_child_name);
        loadingMap = view.findViewById(R.id.loadingMap);

        RecyclerView.LayoutManager mng = new GridLayoutManager(getContext(), 1);
        userListRecyclerView.setLayoutManager(mng);
        usersAdapter = new UsersAdapter(usersKeys, getActivity(), getContext(), firebaseDatabase, databaseReference);
        userListRecyclerView.setAdapter(usersAdapter);


        btn_send_message_cto_child2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityUtils.getTopActivity(), DirectMessage.class);
                intent.putExtra("userID", toUserID);
                startActivity(intent);

            }
        });

        btn_gps_update2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                loadingMap.setVisibility(View.VISIBLE);

                Map gpsMap = new HashMap();
                gpsMap.put("status", "1");
                gpsMap.put("type", "1");

                FirebaseHelper.getDatabaseReference()
                        .child("Users")
                        .child(toUserID)
                        .child("action")
                        .setValue(gpsMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {


                        FirebaseHelper.getDatabaseReference()
                                .child("Users")
                                .child(toUserID)
                                .child("userData")
                                .child("gps").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                gpsCheckerCount++;

                                if (gpsCheckerCount > 0) {

                                    LogUtils.dTag("harita", dataSnapshot.getValue());
                                    lat = (double) dataSnapshot.child("lat").getValue();
                                    lng = (double) dataSnapshot.child("long").getValue();

                                    mapView.onResume();
                                    mapView.getMapAsync(HomePage.this); //onMapReady tetiklenmesi için

                                }

                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });

                    }
                });


            }
        });


    }


}
