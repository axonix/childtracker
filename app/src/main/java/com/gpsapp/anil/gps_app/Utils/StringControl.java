package com.gpsapp.anil.gps_app.Utils;

import android.text.TextUtils;

public class StringControl {

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }
}
