package com.gpsapp.anil.gps_app.Activities;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.StringUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.gpsapp.anil.gps_app.R;
import com.gpsapp.anil.gps_app.Utils.AlertUtils;
import com.gpsapp.anil.gps_app.Utils.RandomStringGenerator;
import com.gpsapp.anil.gps_app.Utils.StringControl;
import com.irozon.sneaker.Sneaker;

import java.util.HashMap;
import java.util.Map;


public class Register extends AppCompatActivity {

    EditText user_input_email, user_input_pass;
    Button btn_user_register;
    FirebaseAuth firebaseAuth;
    TextView already_have_account;
    DatabaseReference databaseReference;
    FirebaseDatabase firebaseDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_register);
        Sneaker.with(Register.this);


        init();
    }

    private void init() {

        user_input_email = findViewById(R.id.user_input_email);
        user_input_pass = findViewById(R.id.user_input_pass);
        btn_user_register = findViewById(R.id.btn_user_register);
        already_have_account = findViewById(R.id.already_have_account);

        firebaseAuth = FirebaseAuth.getInstance();


        already_have_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityUtils.startActivity(Register.this, Login.class);
            }
        });

        btn_user_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = user_input_email.getText().toString();
                String password = user_input_pass.getText().toString();

                if (StringControl.isValidEmail(email) && !StringUtils.isEmpty(password)) {
                    register(email, password);
                } else {
                    AlertUtils.warning(ActivityUtils.getTopActivity(), "Register warning...",
                            "Please enter your email or password correctly.");
                }

            }
        });

    }


    public void register(String email, String password) {
        firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {

                    firebaseDatabase = FirebaseDatabase.getInstance();
                    databaseReference = firebaseDatabase.getReference().child("Users").child(firebaseAuth.getUid());

                    Map map = new HashMap();
                    map.put("userPP", RandomStringGenerator.getHandShakeKey());
                    map.put("name", RandomStringGenerator.getHandShakeKey());
                    map.put("edu", RandomStringGenerator.getHandShakeKey());
                    map.put("dateOfBirth", RandomStringGenerator.getHandShakeKey());
                    map.put("about", RandomStringGenerator.getHandShakeKey());
                    map.put("handshake", "");


                    databaseReference.setValue(map);




                    ActivityUtils.startActivity(Register.this, SelectUserType.class);
                } else {
                    LogUtils.dTag("firebaseException",task.getException());
                    AlertUtils.warning(ActivityUtils.getTopActivity(), "Register warning.",
                            "Please enter your email or password correctly.");
                }
            }
        });

    }
}
