package com.gpsapp.anil.gps_app.Models;

public class DirectMessageModel {

    String type,time,messageText,from;

    Boolean seen;

    public DirectMessageModel(String type, String time, String messageText, String from, Boolean seen) {
        this.type = type;
        this.time = time;
        this.messageText = messageText;
        this.from = from;
        this.seen = seen;
    }

    public DirectMessageModel() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public Boolean getSeen() {
        return seen;
    }

    public void setSeen(Boolean seen) {
        this.seen = seen;
    }



}
