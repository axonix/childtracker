package com.gpsapp.anil.gps_app.Adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.blankj.utilcode.util.LogUtils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.gpsapp.anil.gps_app.Activities.DirectMessage;
import com.gpsapp.anil.gps_app.Fragments.UserProfile;
import com.gpsapp.anil.gps_app.Models.Users;
import com.gpsapp.anil.gps_app.R;
import com.gpsapp.anil.gps_app.Utils.FirebaseHelper;
import com.gpsapp.anil.gps_app.Utils.frgUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.blankj.utilcode.util.ActivityUtils.startActivity;


public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.ViewHolder> {



    List<String> usersKeys;
    Activity activity;
    Context context;
    Button btn_send_message_cto_child,btn_gps_update;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    ConstraintLayout item_users_outlayout;

    public UsersAdapter(List<String> usersKeys, Activity activity, Context context, FirebaseDatabase firebaseDatabase,
                        DatabaseReference databaseReference) {
        this.usersKeys = usersKeys;
        this.activity = activity;
        this.context = context;
        this.firebaseDatabase = firebaseDatabase;
        this.databaseReference = databaseReference;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_users, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, @SuppressLint("RecyclerView") final int i) {

        databaseReference.child("Users").child(usersKeys.get(i).toString()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Users users = dataSnapshot.getValue(Users.class);

                //Picasso.get().load(users.getUserPP()).into(viewHolder.user_image_list);
                viewHolder.username_list.setText(users.getName().toString());

                viewHolder.username_list.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(activity, "users item clicked", Toast.LENGTH_SHORT).show();
                        frgUtils frgUtils = new frgUtils(context);
                        frgUtils.changeWithData(new UserProfile(), "userID", usersKeys.get(i));


                    }
                });

                btn_send_message_cto_child.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(activity, DirectMessage.class);
                        intent.putExtra("userID", usersKeys.get(i).toString());
                        startActivity(intent);

                    }
                });

                btn_gps_update.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        FirebaseHelper.getDatabaseReference()
                                .child("Users")
                                .child(usersKeys.get(i).toString())
                                .child("userData")
                                .child("gps").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });

                    }
                });

            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return usersKeys.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        TextView username_list;
        CircleImageView user_image_list;


        ViewHolder(View itemView) {
            super(itemView);
            username_list = itemView.findViewById(R.id.username_list);
            user_image_list = itemView.findViewById(R.id.user_image_list);
            item_users_outlayout = itemView.findViewById(R.id.item_users_outlayout);
            btn_send_message_cto_child= itemView.findViewById(R.id.btn_send_message_cto_child);
            btn_gps_update= itemView.findViewById(R.id.btn_gps_update);

        }
    }
}