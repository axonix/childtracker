package com.gpsapp.anil.gps_app.Services;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.blankj.utilcode.util.LogUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.gpsapp.anil.gps_app.Activities.MainActivity;
import com.gpsapp.anil.gps_app.R;
import com.gpsapp.anil.gps_app.Services.Actions.BatteryStatus;
import com.gpsapp.anil.gps_app.Services.Actions.Gps;
import com.gpsapp.anil.gps_app.Services.Actions.InstalledAppsChild;
import com.gpsapp.anil.gps_app.Services.Actions.MessageNotification;
import com.gpsapp.anil.gps_app.Services.Actions.SendAlarm;
import com.gpsapp.anil.gps_app.Services.Actions.VoiceRecord;

import java.io.IOException;

public class YourService extends Service {

    private static final int NOTIF_ID = 1;
    private static final String NOTIF_CHANNEL_ID = "1";
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    FirebaseAuth firebaseAuth;
    FirebaseUser firebaseUser;
    Boolean isNowRecorded = false;
    String actionType = "0";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();


        if (firebaseUser != null) {

            databaseReference
                    .child("Users")
                    .child(firebaseUser.getUid())
                    .child("action")
                    .addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull final DataSnapshot dataSnapshotAction) {



                            if (dataSnapshotAction.child("status").getValue().equals("1")) {
                                LogUtils.dTag("tetik", "tetik1");


                                databaseReference.child("Users")
                                        .child(firebaseUser.getUid())
                                        .child("handshake")
                                        .child("userType").addValueEventListener(new ValueEventListener() {

                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.getValue().toString().equals("parent")) {

                                            actionType = dataSnapshotAction.child("type").getValue().toString();

                                            LogUtils.dTag("aksiyon", actionType);

                                            switch (actionType) {


                                                case "6": //Send Alarm
                                                    SendAlarm.sendAlarmToChild();
                                                    turnOffAction();

                                                    break;


                                                case "7": //Send Notification
                                                    LogUtils.dTag("bildirimdeneme","bildirim");
                                                    MessageNotification.showNotification(getApplicationContext(),
                                                            "Message","You have a new message from your child.",
                                                            new Intent());
                                                    turnOffAction();

                                                    break;

                                                default: //Default


                                                    break;
                                            }

                                        } else {
                                            actionType = dataSnapshotAction.child("type").getValue().toString();

                                            LogUtils.dTag("aksiyon", actionType+" child");

                                            switch (actionType) {
                                                case "1": //GPS
                                                    Gps.pushToDB();
                                                    turnOffAction();
                                                    break;

                                                case "2": //Voice Record
                                                    VoiceRecord.record();
                                                    isNowRecorded = true;

                                                    break;

                                                case "3": //Battery Status
                                                    BatteryStatus.pushToDB();
                                                    turnOffAction();

                                                    break;

                                                case "4": //Installed Apps
                                                    InstalledAppsChild.pushToDB();
                                                    turnOffAction();

                                                    break;

                                                case "5": //Running Apps
                                                    InstalledAppsChild.runningApps();
                                                    turnOffAction();

                                                    break;

                                                case "6": //Send Alarm
                                                    SendAlarm.sendAlarmToChild();
                                                    turnOffAction();

                                                    break;


                                                case "7": //Send Notification
                                                    LogUtils.dTag("bildirimdeneme","bildirim");
                                                    MessageNotification.showNotification(getApplicationContext(),
                                                            "Message","You have a new message from your child.",
                                                            new Intent());
                                                    turnOffAction();

                                                    break;

                                                default: //Default
                                                    Gps.pushToDB();
                                                    turnOffAction();

                                                    break;
                                            }

                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });


                                Log.d("servisdurumu", "Servis tetiklendi. =>"
                                        + dataSnapshotAction.getValue().toString());
                            } else {

                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });


        }


        startForeground();
        return super.onStartCommand(intent, flags, startId);
    }

    public void turnOffAction() {
        databaseReference
                .child("Users")
                .child(firebaseUser.getUid())
                .child("action")
                .child("status")
                .setValue("0")
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                    }
                });
    }

    private void startForeground() {
        Intent notificationIntent = new Intent(this, MainActivity.class);

     /*   PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);

        startForeground(NOTIF_ID, new NotificationCompat.Builder(this,
                NOTIF_CHANNEL_ID) // don't forget create a notification channel first
                .setOngoing(true)
                .setSmallIcon(R.drawable.ic_map_statusbar)
                .setContentTitle(getString(R.string.app_name))
                .setContentText("Service is running background")
                .setContentIntent(pendingIntent)
                .build());*/
    }
}