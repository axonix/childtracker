package com.gpsapp.anil.gps_app.Activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.StringUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.gpsapp.anil.gps_app.R;
import com.gpsapp.anil.gps_app.Utils.AlertUtils;
import com.gpsapp.anil.gps_app.Utils.StringControl;
import com.irozon.sneaker.Sneaker;

public class Login extends AppCompatActivity {
    EditText user_input_email, user_input_pass;
    Button btn_user_login;
    FirebaseAuth firebaseAuth;
    TextView newUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();

        setContentView(R.layout.activity_login);
        Sneaker.with(Login.this);
        init();

    }

    private void init() {

        user_input_email = findViewById(R.id.user_input_email_login);
        user_input_pass = findViewById(R.id.user_input_pass_login);
        btn_user_login = findViewById(R.id.btn_user_register);
        newUser = findViewById(R.id.new_user);

        firebaseAuth = FirebaseAuth.getInstance();

        newUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityUtils.startActivity(Login.this,Register.class);
            }
        });

        btn_user_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = user_input_email.getText().toString();
                String password = user_input_pass.getText().toString();

                if (StringControl.isValidEmail(email) && !StringUtils.isEmpty(password)) {
                    login(email, password);
                } else {
                    AlertUtils.warning(ActivityUtils.getTopActivity(), "Login warning...",
                            "Please enter your email or password correctly.");
                }

            }
        });

    }


    public void login(String email, String password) {
        firebaseAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    ActivityUtils.startActivity(Login.this, MainActivity.class);
                } else {
                    AlertUtils.warning(ActivityUtils.getTopActivity(), "Login warning.",
                            "Please enter your email or password correctly.");
                }
            }
        });

    }
}
