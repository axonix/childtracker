package com.gpsapp.anil.gps_app.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.gpsapp.anil.gps_app.R;

public class RunningApps extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();

        setContentView(R.layout.activity_running_apps);
    }
}
