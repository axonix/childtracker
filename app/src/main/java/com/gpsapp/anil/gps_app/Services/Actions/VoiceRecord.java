package com.gpsapp.anil.gps_app.Services.Actions;

import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;

import com.blankj.utilcode.util.LogUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.gpsapp.anil.gps_app.Utils.FirebaseHelper;
import com.gpsapp.anil.gps_app.Utils.L;
import com.gpsapp.anil.gps_app.Utils.RandomStringGenerator;
import com.gpsapp.anil.gps_app.Utils.TimeUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class VoiceRecord {

    private static MediaRecorder recorder;
    private static String voicePath;
    private static String randomFileName;
    private static FirebaseStorage firebaseStorage;
    private static StorageReference storageReference;
    private static Map voiceMap;


    public static void record() {

        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        randomFileName = RandomStringGenerator.getSaltString();
        recorder.setOutputFile(Environment.getExternalStorageDirectory()
                .getAbsolutePath() + "/" + randomFileName + ".mp3");
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        try {
            recorder.prepare();
            voicePath = Environment.getExternalStorageDirectory()
                    .getAbsolutePath() + "/" + randomFileName + ".mp3";

        } catch (IOException e) {
            e.printStackTrace();
        }

        recorder.start();


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                recorder.stop();
                pushToDB(voicePath, randomFileName);
            }
        }, TimeUnit.SECONDS.toMillis(15));//millisec.

    }


    public static void turnOffAction() {
        FirebaseHelper.getDatabaseReference()
                .child("Users")
                .child(FirebaseHelper.getFirebaseUser().getUid())
                .child("action")
                .child("status")
                .setValue("0")
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                    }
                });
    }


    private static void pushToDB(final String voiceUri, String fileName) {
        firebaseStorage = FirebaseStorage.getInstance();
        storageReference = firebaseStorage.getReference();
        final Uri uri = Uri.fromFile(new File(voiceUri));

        LogUtils.dTag("voicekayit", uri.toString());
        final StorageReference ref = storageReference.child("UsersVoiceRecords").child(fileName + ".mp3");
        ref.putFile(uri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {

                if (task.isSuccessful()) {

                    ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {

                            Uri dlUri = uri;

                            voiceMap = new HashMap();

                            voiceMap.put("voiceURL", dlUri.toString());
                            voiceMap.put("time", TimeUtils.getCurrentTimeUsingCalendar());

                            FirebaseHelper.getDatabaseReference()
                                    .child("Users")
                                    .child(FirebaseHelper.getFirebaseUser().getUid())
                                    .child("userData")
                                    .child("voiceRecord")
                                    .setValue(voiceMap)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {

                                            turnOffAction();

                                        }
                                    });
                        }
                    });

                } else {
                    L.err(task.getException().getMessage());
                }
            }
        });


    }


}
