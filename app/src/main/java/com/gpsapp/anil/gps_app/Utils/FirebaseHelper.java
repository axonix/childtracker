package com.gpsapp.anil.gps_app.Utils;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class FirebaseHelper {


    static FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
    static DatabaseReference databaseReference = firebaseDatabase.getReference();
    static FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    static FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();

    public static FirebaseDatabase getFirebaseDatabase() {
        return firebaseDatabase;
    }

    public static DatabaseReference getDatabaseReference() {
        return databaseReference;
    }

    public static FirebaseAuth getFirebaseAuth() {
        return firebaseAuth;
    }

    public static FirebaseUser getFirebaseUser() {
        return firebaseUser;
    }


    public static String getMyID(){
        return firebaseUser.getUid();
    }



}
