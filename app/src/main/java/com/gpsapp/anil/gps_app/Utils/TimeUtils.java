package com.gpsapp.anil.gps_app.Utils;

import android.annotation.TargetApi;
import android.icu.text.SimpleDateFormat;
import android.os.Build;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

public class TimeUtils {
    @TargetApi(Build.VERSION_CODES.N) //TODO api farklılığı. Test
    public static final String getCurrentDate(){
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        String currentDateandTime = sdf.format(new Date());
        return  currentDateandTime;
    }
    public static String getCurrentTimeUsingCalendar() {
        Calendar cal = Calendar.getInstance();
        Date date = cal.getTime();
        DateFormat dateFormat = new java.text.SimpleDateFormat("HH:mm:ss");
        return dateFormat.format(date);

    }
}
