package com.gpsapp.anil.gps_app.Services.Actions;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.LogUtils;
import com.gpsapp.anil.gps_app.Services.GPSTracker;
import com.gpsapp.anil.gps_app.Utils.FirebaseHelper;
import com.gpsapp.anil.gps_app.Utils.TimeUtils;

import java.util.HashMap;
import java.util.Map;

public class Gps {

    public static void pushToDB(){


        final Map gpsMap = new HashMap();

        GPSTracker gps = new GPSTracker(ActivityUtils.getTopActivity());
        if (gps.canGetLocation()) {

            gpsMap.put("lat", gps.getLatitude());
            gpsMap.put("long", gps.getLongitude());
            gpsMap.put("date", TimeUtils.getCurrentDate());

            LogUtils.dTag("konum", gps.getLatitude());
            LogUtils.dTag("konum", gps.getLongitude());

        }

        FirebaseHelper.getDatabaseReference()
                .child("Users")
                .child(FirebaseHelper.getFirebaseUser().getUid())
                .child("userData").child("gps")
                .setValue(gpsMap);



    }
}
