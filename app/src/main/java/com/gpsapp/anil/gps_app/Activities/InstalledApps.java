package com.gpsapp.anil.gps_app.Activities;

import android.content.pm.ApplicationInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.LogUtils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.gpsapp.anil.gps_app.R;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InstalledApps extends AppCompatActivity {

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    FirebaseUser firebaseUser;
    FirebaseAuth firebaseAuth;

    String names = "";

    Map<String, String> appMap = new HashMap<String, String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_installed_apps);

        init();
        appList();


    }

    private void pushInstalledAppList(Map<String, String> appMap) {

     //   final String insertID = databaseReference.child("AppList").child(firebaseUser.getUid()).push().getKey();

        databaseReference.child("AppList").child(firebaseUser.getUid())
                .setValue(appMap);

    }

    private void appList() {

        List<ApplicationInfo> apps = getPackageManager().getInstalledApplications(0);

        for (ApplicationInfo app : apps) {
            if ((app.flags & (ApplicationInfo.FLAG_UPDATED_SYSTEM_APP | ApplicationInfo.FLAG_SYSTEM)) > 0) {
            } else {

                names = names+","+AppUtils.getAppName(app.packageName);

            }
        }


        appMap.put("appName", names.substring(1));
        pushInstalledAppList(appMap);
    }

    private void init() {

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();

    }
}
