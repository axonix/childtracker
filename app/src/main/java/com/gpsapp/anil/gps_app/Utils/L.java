package com.gpsapp.anil.gps_app.Utils;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.LogUtils;

public class L {

    private static String TAG = "gpstakip";

    public static void err(String errorMessage) {
        LogUtils.eTag(TAG, errorMessage);
    }

    public static void log(String log) {
        LogUtils.dTag(TAG, log);
    }

    public static void firebaseFailure(String failureMessage,int errorCode){
        AlertUtils.error(ActivityUtils.getTopActivity(),"Geçici Hata"
                ,"Sistemde geçiçi bir sorun oluştu. Lütfen daha sonra tekrar deneyin.");
        err("Error = "+failureMessage+"  \nError Code= "+errorCode);
    }


}
