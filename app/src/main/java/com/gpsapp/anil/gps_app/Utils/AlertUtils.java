package com.gpsapp.anil.gps_app.Utils;

import android.app.Activity;
import com.irozon.sneaker.Sneaker;

public class AlertUtils {


    public static void warning(Activity act, String title, String description) {
        Sneaker.with(act)
                .setTitle(title)
                .setMessage(description)
                .sneakWarning();
    }

    public static void success(Activity act, String title, String description) {
        Sneaker.with(act)
                .setTitle(title)
                .setMessage(description)
                .sneakSuccess();
    }

    public static void error(Activity act, String title, String description) {
        Sneaker.with(act)
                .setTitle(title)
                .setMessage(description)
                .sneakError();
    }


}

