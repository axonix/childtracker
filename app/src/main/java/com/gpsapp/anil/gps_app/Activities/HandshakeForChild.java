package com.gpsapp.anil.gps_app.Activities;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.blankj.utilcode.util.ActivityUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.gpsapp.anil.gps_app.R;
import com.gpsapp.anil.gps_app.Utils.AlertUtils;
import com.gpsapp.anil.gps_app.Utils.FirebaseHelper;
import com.gpsapp.anil.gps_app.Utils.RandomStringGenerator;

public class HandshakeForChild extends AppCompatActivity {


    String keyFromDB;
    EditText handshake_key_from_child;
    Button btn_check_key;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_handshake_for_child);

        init();
        action();

    }

    private void action() {
        btn_check_key.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkKey();
            }
        });
    }

    private void init() {


        handshake_key_from_child = findViewById(R.id.handshake_key_from_child);
        btn_check_key = findViewById(R.id.btn_check_key);


    }

    private void checkKey() {
        Query query = FirebaseHelper.getFirebaseDatabase()
                .getReference("Users")
                .orderByChild("handshake/key")
                .equalTo(handshake_key_from_child.getText().toString());

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {

                    keyFromDB = ds.child("handshake").child("key").getValue().toString();

                    if (keyFromDB.equals(handshake_key_from_child.getText().toString())) {
                        changeKeyForParent(ds.getKey());
                    } else {
                        AlertUtils.warning(ActivityUtils.getTopActivity(), "Doğrulama.",
                                "Doğrulama Yapılamadı. Key Geçersiz");
                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void changeKeyForParent(final String userID) {

        FirebaseHelper.getDatabaseReference()
                .child("Users")
                .child(userID)
                .child("handshake")
                .child("key").setValue(RandomStringGenerator.getHandShakeKey()).addOnCompleteListener(new OnCompleteListener<Void>() {

            @Override
            public void onComplete(@NonNull Task<Void> task) {

                FirebaseHelper.getDatabaseReference()
                        .child("Users")
                        .child(userID)
                        .child("handshake")
                        .child("relationship").setValue(FirebaseHelper.getFirebaseUser().getUid()).addOnCompleteListener(new OnCompleteListener<Void>() {

                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            AlertUtils.success(ActivityUtils.getTopActivity(), "Doğrulama.",
                                    "Doğrulama Başarılı");
                            ActivityUtils.startActivity(HandshakeForChild.this, ChildPanel.class);

                        }
                    }
                });

            }
        });

    }
}
