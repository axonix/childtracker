package com.gpsapp.anil.gps_app.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.StringUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.gpsapp.anil.gps_app.Models.Users;
import com.gpsapp.anil.gps_app.R;
import com.gpsapp.anil.gps_app.Utils.AlertUtils;
import com.gpsapp.anil.gps_app.Utils.RandomStringGenerator;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyProfile extends Fragment {


    Boolean isChangePP =false;
    FirebaseAuth firebaseAuth;
    FirebaseUser firebaseUser;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    String imageURL;
    View view;
    EditText user_profile_name, user_profile_dateofbirth, user_profile_edu, user_profile_about;
    TextView txt_user_name;
    Button profile_save, imgChange;
    CircleImageView userPP;

    Map<String, String> userProfileMap = new HashMap<>();

    FirebaseStorage firebaseStorage;
    StorageReference storageReference;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_my_profile, container, false);
        init();
        getProfileData();
        return view;
    }

    public void init() {

        user_profile_name = view.findViewById(R.id.user_profile_name);
        user_profile_dateofbirth = view.findViewById(R.id.user_profile_dateofbirth);
        user_profile_edu = view.findViewById(R.id.user_profile_edu);
        user_profile_about = view.findViewById(R.id.user_profile_about);
        profile_save = view.findViewById(R.id.profile_save);
        imgChange = view.findViewById(R.id.imgChange);
        userPP = view.findViewById(R.id.user_pp);
        txt_user_name = view.findViewById(R.id.txt_user_name);

        firebaseStorage = FirebaseStorage.getInstance();
        storageReference = firebaseStorage.getReference();


        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("Users").child(firebaseUser.getUid());

        profile_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateProfileData();
            }
        });

        userPP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImage();

            }
        });

        imgChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImage();

            }
        });


    }


    public void onActivityResult(int reqCode, int resultCode, Intent data) {

        if (reqCode == 5 && resultCode == Activity.RESULT_OK) {
            final Uri filePath = data.getData();
            LogUtils.dTag("resim", filePath.toString());
            final StorageReference ref = storageReference.child("UsersProfileImages").child(RandomStringGenerator.getSaltString() + ".jpg");
            ref.putFile(filePath).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {


                    if (task.isSuccessful()) {


                        ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                Uri dlUri = uri;


                                LogUtils.dTag("resimup", dlUri);

                                isChangePP = true;


                                userProfileMap.put("userPP", dlUri.toString());

                                updateProfileData();

                            }
                        });


                    } else {
                        LogUtils.dTag("resimup", task.getException());

                    }
                }
            });

        }
    }

    private void chooseImage() {

        Intent imageChoose = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(imageChoose, 5);


    }

    public void getProfileData() {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                Users users = dataSnapshot.getValue(Users.class);


                user_profile_name.setText(users.getName());
                user_profile_dateofbirth.setText(users.getDateOfBirth());
                user_profile_edu.setText(users.getEdu());
                user_profile_about.setText(users.getAbout());
                txt_user_name.setText(users.getName());
                if(StringUtils.isEmpty(users.getUserPP())){
                    imageURL = "null";
                }else{
                    imageURL = users.getUserPP();
                }
                Picasso.get().load(users.getUserPP()).into(userPP);


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    public void updateProfileData() {
        String name = user_profile_name.getText().toString();
        String dateofbirth = user_profile_dateofbirth.getText().toString();
        String edu = user_profile_edu.getText().toString();
        String about = user_profile_about.getText().toString();

        databaseReference = firebaseDatabase.getReference().child("Users").child(firebaseAuth.getUid());


        userProfileMap.put("name", name);
        userProfileMap.put("edu", edu);
        userProfileMap.put("dateOfBirth", dateofbirth);
        userProfileMap.put("about", about);

        if (imageURL.equals("null") && !isChangePP) {
            userProfileMap.put("userPP", "null");
        }


        databaseReference.setValue(userProfileMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {

                    AlertUtils.success(ActivityUtils.getTopActivity(), "Profile Data Updated.",
                            "Your profile information has been successfully updated.");
                } else {
                    AlertUtils.warning(ActivityUtils.getTopActivity(), "Failed Profile Data Updated.",
                            "Your profile information has been failed updated.");
                }
            }
        });

    }

}
