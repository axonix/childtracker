package com.gpsapp.anil.gps_app.Services.Actions;

import android.media.MediaPlayer;
import android.os.Handler;

import com.blankj.utilcode.util.ActivityUtils;
import com.gpsapp.anil.gps_app.R;

import java.util.concurrent.TimeUnit;

public class SendAlarm {

    private static MediaPlayer mp;


    public static void sendAlarmToChild() {
        stopPlaying();
        mp = MediaPlayer.create(ActivityUtils.getTopActivity(), R.raw.alarm);
        mp.setLooping(true);
        mp.setVolume(1f, 1f);

        mp.start();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mp.stop();
            }
        }, TimeUnit.SECONDS.toMillis(10));//millisec.
    }


    public static void sendAlarmToParent() {

    }

    public static void stopPlaying() {
        if (mp != null) {
            mp.stop();
            mp.release();
            mp = null;
        }
    }
}
