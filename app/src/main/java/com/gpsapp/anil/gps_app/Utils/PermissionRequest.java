package com.gpsapp.anil.gps_app.Utils;

import android.Manifest;
import android.annotation.SuppressLint;

import com.blankj.utilcode.util.PermissionUtils;

public class PermissionRequest {

    @SuppressLint("WrongConstant")
    public static void reqPerm(){

        PermissionUtils.permission(Manifest.permission.ACCESS_FINE_LOCATION).request(); //bunları activit üzerinden çağır.
        PermissionUtils.permission(Manifest.permission.RECORD_AUDIO).request();
        PermissionUtils.permission(Manifest.permission.CAMERA).request();
        PermissionUtils.permission(Manifest.permission.WRITE_EXTERNAL_STORAGE).request();
        PermissionUtils.permission(Manifest.permission.INTERNET).request();
        PermissionUtils.permission(Manifest.permission.ACCESS_NETWORK_STATE).request();
        PermissionUtils.permission(Manifest.permission.ACCESS_WIFI_STATE).request();
        PermissionUtils.permission(Manifest.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS).request();
        PermissionUtils.permission(Manifest.permission.FOREGROUND_SERVICE).request();
        PermissionUtils.permission(Manifest.permission.ACCESS_FINE_LOCATION).request();
        PermissionUtils.permission(Manifest.permission.ACCESS_COARSE_LOCATION).request();
        PermissionUtils.permission(Manifest.permission.FOREGROUND_SERVICE).request();
    }


    //Batarya opt kapatma uyarısı
       /* if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Intent intent = new Intent();
            String packageName = getPackageName();
            PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
            if (!pm.isIgnoringBatteryOptimizations(packageName)) {
                intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                intent.setData(Uri.parse("package:" + packageName));
                startActivity(intent);
            }
        }*/
}
