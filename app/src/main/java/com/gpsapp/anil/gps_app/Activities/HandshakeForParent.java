package com.gpsapp.anil.gps_app.Activities;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.LogUtils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.gpsapp.anil.gps_app.R;
import com.gpsapp.anil.gps_app.Utils.AlertUtils;

public class HandshakeForParent extends AppCompatActivity {
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    FirebaseUser firebaseUser;
    FirebaseAuth firebaseAuth;
    TextView parentHandshakeCode;

    String key;

    int count = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_handshake_for_parent);


        init();
        action();
    }

    private void init() {

        //firebase define
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("Users");
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();

        //views define
        parentHandshakeCode = findViewById(R.id.parentHandshakeCode);


    }

    private void action() {


        databaseReference.child(firebaseUser.getUid()).child("handshake").child("key").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                key = dataSnapshot.getValue().toString();
                count++;
                parentHandshakeCode.setText(key);

                if (count > 1) {
                    LogUtils.dTag("kaç", "Tetik  " + count + "  " + key);
                    AlertUtils.success(ActivityUtils.getTopActivity(), "Bağlantı.",
                            "Bağlantı Kuruldu..");






                    ActivityUtils.startActivity(HandshakeForParent.this, MainActivity.class);
                }
                LogUtils.dTag("kaç", "Tetik  " + count + "  " + key);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }
}
