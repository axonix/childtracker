package com.gpsapp.anil.gps_app.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.blankj.utilcode.util.ActivityUtils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.gpsapp.anil.gps_app.R;
import com.gpsapp.anil.gps_app.Utils.AlertUtils;
import com.gpsapp.anil.gps_app.Utils.FirebaseHelper;
import com.gpsapp.anil.gps_app.Utils.L;

import java.util.HashMap;
import java.util.Map;

public class ChildPanel extends AppCompatActivity {

    String idFromDB;
    Button btn_send_message_to_parent,btn_send_alarm;
    Button quickmessage1, quickmessage2, quickmessage3, quickmessage4, quickmessage5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_child_panel);

        init();
        quickmessageAction();

    }

    private void init() {

        btn_send_message_to_parent = findViewById(R.id.btn_send_message_to_parent);
        btn_send_alarm = findViewById(R.id.btn_send_alarm);

        quickmessage1 = findViewById(R.id.quickmessage1);
        quickmessage2 = findViewById(R.id.quickmessage2);
        quickmessage3 = findViewById(R.id.quickmessage3);
        quickmessage4 = findViewById(R.id.quickmessage4);
        quickmessage5 = findViewById(R.id.quickmessage5);

        btn_send_message_to_parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(ChildPanel.this, DirectMessage.class);
                startMessages(intent);

            }
        });

        btn_send_alarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Query query = FirebaseHelper.getFirebaseDatabase()
                        .getReference("Users")
                        .orderByChild("handshake/relationship")
                        .equalTo(FirebaseHelper.getFirebaseUser().getUid());

                query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot ds : dataSnapshot.getChildren()) {

                            idFromDB = ds.child("handshake").child("relationship").getValue().toString();

                            Map map = new HashMap();
                            map.put("status","1");
                            map.put("type","6");

                            if (idFromDB.equals(FirebaseHelper.getFirebaseUser().getUid())) {

                              FirebaseHelper.getDatabaseReference()
                                      .child("Users")
                                      .child(ds.getKey())
                                      .child("action")
                                      .setValue(map);

                            } else {
                                AlertUtils.warning(ActivityUtils.getTopActivity(), "Hata.",
                                        "Bilinmeyen nedenden dolayı bir hata oluştu.");
                                L.err("ID bulunamadı.");
                            }

                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        L.firebaseFailure(databaseError.getMessage(), databaseError.getCode());
                    }
                });


            }
        });

    }

    public void startMessages(final Intent intent) {

        Query query = FirebaseHelper.getFirebaseDatabase()
                .getReference("Users")
                .orderByChild("handshake/relationship")
                .equalTo(FirebaseHelper.getFirebaseUser().getUid());

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {

                    idFromDB = ds.child("handshake").child("relationship").getValue().toString();

                    if (idFromDB.equals(FirebaseHelper.getFirebaseUser().getUid())) {

                        L.log(idFromDB + " idFromDB <=> firebaseUser.getUid " + FirebaseHelper.getFirebaseUser().getUid());

                        intent.putExtra("userID", ds.getKey());
                        startActivity(intent);
                    } else {
                        AlertUtils.warning(ActivityUtils.getTopActivity(), "Hata.",
                                "Bilinmeyen nedenden dolayı bir hata oluştu.");
                        L.err("ID bulunamadı.");
                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                L.firebaseFailure(databaseError.getMessage(), databaseError.getCode());
            }
        });
    }

    private void quickmessageAction() {

        quickmessage1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startIntentWithData(quickmessage1);

            }
        });
        quickmessage2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startIntentWithData(quickmessage2);

            }
        });
        quickmessage3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startIntentWithData(quickmessage3);

            }
        });
        quickmessage4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startIntentWithData(quickmessage4);

            }
        });
        quickmessage5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startIntentWithData(quickmessage5);
            }
        });

    }

    public void startIntentWithData(Button QuickMessageButton) {
        Intent intent = new Intent(ChildPanel.this, DirectMessage.class);
        intent.putExtra("quickMessageText", QuickMessageButton.getText().toString());
        startMessages(intent);
    }
}
