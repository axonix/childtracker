package com.gpsapp.anil.gps_app.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.LogUtils;
import com.elconfidencial.bubbleshowcase.BubbleShowCaseBuilder;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.gpsapp.anil.gps_app.Activities.DirectMessage;
import com.gpsapp.anil.gps_app.Models.Users;
import com.gpsapp.anil.gps_app.R;


public class UserProfile extends Fragment {

    View view;
    TextView profil_txtUserNameSub, profil_txtUserEdu, profil_txtUserDataOfBirth, profil_txtUserAbout,
            profil_txtUserName;

    Button btn_send_message;

    String userID;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_user_profile, container, false);

        init();
        action();

        return view;
    }


    private void init() {


        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();

        userID = getArguments().getString("userID");
        LogUtils.dTag("userID", userID);

        profil_txtUserNameSub = view.findViewById(R.id.profil_txtUserNameSub);
        profil_txtUserName = view.findViewById(R.id.profil_txtUserName);
        profil_txtUserEdu = view.findViewById(R.id.profil_txtUserEdu);
        profil_txtUserDataOfBirth = view.findViewById(R.id.profil_txtUserDataOfBirth);
        profil_txtUserAbout = view.findViewById(R.id.profil_txtUserAbout);

        btn_send_message = view.findViewById(R.id.btn_send_message);


        btn_send_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), DirectMessage.class);
                intent.putExtra("userID", userID);
                startActivity(intent);

            }
        });


        new BubbleShowCaseBuilder(getActivity()) //Activity instance
                .title("Buradan mesaj gönderebilirsiniz.") //Any title for the bubble view
                .targetView(btn_send_message) //View to point out
                .show(); //Display the ShowCase


    }


    private void action() {

        databaseReference.child("Users").child(userID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Users users = dataSnapshot.getValue(Users.class);

                profil_txtUserName.setText(users.getName());
                profil_txtUserNameSub.setText(users.getName());
                profil_txtUserEdu.setText(users.getEdu());
                profil_txtUserDataOfBirth.setText(users.getDateOfBirth());
                profil_txtUserAbout.setText(users.getAbout());


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }


}
