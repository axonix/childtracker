package com.gpsapp.anil.gps_app.Activities;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.constant.PermissionConstants;
import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.PermissionUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.gpsapp.anil.gps_app.Adapters.DirectMessageAdapter;
import com.gpsapp.anil.gps_app.Models.DirectMessageModel;
import com.gpsapp.anil.gps_app.Models.Users;
import com.gpsapp.anil.gps_app.R;
import com.gpsapp.anil.gps_app.Utils.AlertUtils;
import com.gpsapp.anil.gps_app.Utils.FirebaseHelper;
import com.gpsapp.anil.gps_app.Utils.L;
import com.gpsapp.anil.gps_app.Utils.PermissionRequest;
import com.gpsapp.anil.gps_app.Utils.TimeUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DirectMessage extends AppCompatActivity {

    RecyclerView dm_recycler_view;
    TextView dm_name;
    TextView dm_send_message;
    EditText dm_message_text;
    LinearLayout lyt_dm_back;
    Button btn_dm_back;
    String userID, quickMessageText;
    Users users;
    List<DirectMessageModel> directMessageModelList;
    DirectMessageAdapter directMessageAdapter;
    List<String> keyList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_direct_message);


        init();
        setName();
        loadMessages();





    }


    private void init() {


        Intent intent = getIntent();
        userID = intent.getStringExtra("userID");
        quickMessageText = intent.getStringExtra("quickMessageText");


        directMessageModelList = new ArrayList<>();
        keyList = new ArrayList<>();


        dm_name = findViewById(R.id.dm_name);
        dm_send_message = findViewById(R.id.dm_send_message);
        dm_message_text = findViewById(R.id.dm_message_text);
        btn_dm_back = findViewById(R.id.btn_dm_back);
        dm_message_text.setText(quickMessageText);
        dm_recycler_view = findViewById(R.id.dm_recycler_view);
        lyt_dm_back = findViewById(R.id.lyt_dm_back);

        directMessageAdapter = new DirectMessageAdapter(keyList, DirectMessage.this, DirectMessage.this,
                directMessageModelList);

        dm_recycler_view.setAdapter(directMessageAdapter);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(DirectMessage.this, 1);
        dm_recycler_view.setLayoutManager(layoutManager);

        dm_send_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage(FirebaseHelper.getFirebaseUser().getUid(), userID, "text", TimeUtils.getCurrentTimeUsingCalendar(),
                        true, dm_message_text.getText().toString());
                dm_message_text.setText("");


            }
        });

        btn_dm_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        lyt_dm_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private void sendNotification(String from,String messageText) {


        Map notifyMap = new HashMap();
        notifyMap.put("status","1");
        notifyMap.put("type","7");
/*        notifyMap.put("from",dm_name);
        notifyMap.put("messagetext",dm_message_text);*/

        FirebaseHelper.getDatabaseReference()
                .child("Users")
                .child(userID)
                .child("action")
                .setValue(notifyMap);


    }

    private void sendMessage(final String myID, final String userID, String messageType, String time, Boolean seen, String messageText) {

        final String messageID = FirebaseHelper.getDatabaseReference()
                .child("Messages")
                .child(myID)
                .child(userID)
                .push()
                .getKey();

        final Map messageMap = new HashMap();

        messageMap.put("type", messageType);
        messageMap.put("time", time);
        messageMap.put("seen", seen);
        messageMap.put("messageText", messageText);
        messageMap.put("from", myID);

        FirebaseHelper.getDatabaseReference()
                .child("Messages")
                .child(myID)
                .child(userID)
                .child(messageID)
                .setValue(messageMap).addOnCompleteListener(new OnCompleteListener<Void>() {

            @Override
            public void onComplete(@NonNull Task<Void> task) {

                FirebaseHelper.getDatabaseReference()
                        .child("Messages")
                        .child(userID)
                        .child(myID)
                        .child(messageID)
                        .setValue(messageMap).addOnCompleteListener(new OnCompleteListener<Void>() {

                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                    }
                });
            }
        });
        sendNotification(dm_name.getText().toString(),dm_message_text.getText().toString());
    }

    private void setName() {

        FirebaseHelper.getDatabaseReference()
                .child("Users")
                .child(userID).addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                users = dataSnapshot.getValue(Users.class);
                dm_name.setText(users.getName());

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    public void loadMessages() {

        FirebaseHelper.getDatabaseReference()
                .child("Messages")
                .child(FirebaseHelper.getFirebaseUser().getUid())
                .child(userID).addChildEventListener(new ChildEventListener() {

            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                DirectMessageModel directMessageModel = dataSnapshot.getValue(DirectMessageModel.class);
                directMessageModelList.add(directMessageModel);
                directMessageAdapter.notifyDataSetChanged();
                keyList.add(dataSnapshot.getKey());
                dm_recycler_view.scrollToPosition(directMessageModelList.size() - 1);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


}
