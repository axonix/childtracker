package com.gpsapp.anil.gps_app.Services.Actions;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;

import com.blankj.utilcode.util.ActivityUtils;
import com.gpsapp.anil.gps_app.Utils.FirebaseHelper;

public class BatteryStatus {


    public static int getBatteryPercentage(Context context) {

        IntentFilter iFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, iFilter);

        int level = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) : -1;
        int scale = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1) : -1;

        float batteryPct = level / (float) scale;

        return (int) (batteryPct * 100);
    }

    public static void pushToDB(){


        FirebaseHelper.getDatabaseReference()
                .child("Users")
                .child(FirebaseHelper.getFirebaseUser().getUid())
                .child("userData")
                .child("battery")
                .child("level")
                .setValue(getBatteryPercentage(ActivityUtils.getTopActivity()));


    }



}
