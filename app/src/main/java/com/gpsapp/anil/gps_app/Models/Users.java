package com.gpsapp.anil.gps_app.Models;

public class Users {

    public String name, userPP, dateOfBirth, about, edu;


    public Users() {
    }

    public Users(String name, String userPP, String dateOfBirth, String about, String edu) {
        this.name = name;
        this.userPP = userPP;
        this.dateOfBirth = dateOfBirth;
        this.about = about;
        this.edu = edu;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserPP() {
        return userPP;
    }

    public void setUserPP(String userPP) {
        this.userPP = userPP;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getEdu() {
        return edu;
    }

    public void setEdu(String edu) {
        this.edu = edu;
    }


    @Override
    public String toString() {
        return "Users{" +
                "name='" + name + '\'' +
                ", userPP='" + userPP + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", about='" + about + '\'' +
                ", edu='" + edu + '\'' +
                '}';
    }
}


