package com.gpsapp.anil.gps_app.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.LogUtils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.gpsapp.anil.gps_app.Fragments.Events;
import com.gpsapp.anil.gps_app.Fragments.HomePage;
import com.gpsapp.anil.gps_app.Fragments.MoreOptions;
import com.gpsapp.anil.gps_app.R;
import com.gpsapp.anil.gps_app.Utils.PermissionRequest;
import com.gpsapp.anil.gps_app.Utils.frgUtils;

public class MainActivity extends AppCompatActivity {

    private TextView mTextMessage;
    frgUtils frgUtils;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    FirebaseAuth firebaseAuth;
    FirebaseUser firebaseUser;
    String toUserID;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    frgUtils = new frgUtils(MainActivity.this);
                    frgUtils.change(new HomePage());
                    return true;
                case R.id.navigation_chat:

                    Intent intent = new Intent(ActivityUtils.getTopActivity(), DirectMessage.class);
                    intent.putExtra("userID", toUserID);
                    startActivity(intent);

                    return true;
                case R.id.navigation_events:

                    frgUtils = new frgUtils(MainActivity.this);
                    frgUtils.change(new Events());

                    return true;
                case R.id.navigation_more:

                    frgUtils = new frgUtils(MainActivity.this);
                    frgUtils.change(new MoreOptions());

                    return true;
            }
            return false;
        }
    };

    private void logout() {
        firebaseAuth.signOut();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();


        setContentView(R.layout.activity_main);


        init();
        route();



        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    public void init() {



        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
    }


    public void route() {

        if (firebaseUser == null) {
            ActivityUtils.startActivity(MainActivity.this, Register.class);
            ActivityUtils.finishActivity(MainActivity.this);
        } else {
            databaseReference.child("Users").child(firebaseUser.getUid()).child("handshake").child("userType").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getValue().toString().equals("parent")) {
                        getChild();
                        frgUtils = new frgUtils(MainActivity.this);
                        frgUtils.change(new HomePage());
                    } else {
                        ActivityUtils.startActivity(MainActivity.this, ChildPanel.class);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        }


    }

    private void getChild() {


        databaseReference.child("Users").child(firebaseAuth.getUid()).child("handshake").child("relationship").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot1) {
                LogUtils.dTag("relationdeneme", dataSnapshot1.getValue());


                databaseReference.child("Users").child(dataSnapshot1.getValue().toString()).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        toUserID = dataSnapshot.getKey();

                    }


                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }
}
