package com.gpsapp.anil.gps_app.Services.Actions;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.util.Log;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.AppUtils;
import com.gpsapp.anil.gps_app.Utils.FirebaseHelper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InstalledAppsChild {

    private static String names = "";
    private static Map<String, String> appMap = new HashMap<String, String>();
    private static Context mContext;


    public static void pushInstalledAppList(Map<String, String> appMap) {

        FirebaseHelper.getDatabaseReference()
                .child("Users")
                .child(FirebaseHelper.getFirebaseUser().getUid())
                .child("userData")
                .child("installedApps")
                .setValue(appMap);
    }
    public static void runningApps() {
        final Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        final List pkgAppsList = ActivityUtils.getTopActivity().getPackageManager().queryIntentActivities( mainIntent, 0);

        Log.d("listdeneme", pkgAppsList.toString());
    }

    public static void pushToDB() {


        List<ApplicationInfo> apps = ActivityUtils.getTopActivity().getPackageManager().getInstalledApplications(0);

        for (ApplicationInfo app : apps) {
            if ((app.flags & (ApplicationInfo.FLAG_UPDATED_SYSTEM_APP | ApplicationInfo.FLAG_SYSTEM)) > 0) {
            } else {

                names = names+","+AppUtils.getAppName(app.packageName);

            }
        }


        appMap.put("appNames", names.substring(1));
        pushInstalledAppList(appMap);
    }
}
