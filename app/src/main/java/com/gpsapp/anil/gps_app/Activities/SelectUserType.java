package com.gpsapp.anil.gps_app.Activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.blankj.utilcode.constant.PermissionConstants;
import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.PermissionUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.gpsapp.anil.gps_app.R;
import com.gpsapp.anil.gps_app.Utils.PermissionRequest;
import com.gpsapp.anil.gps_app.Utils.RandomStringGenerator;

import java.util.HashMap;
import java.util.Map;

public class SelectUserType extends AppCompatActivity {

    Button btn_select_child, btn_select_parent;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    FirebaseAuth firebaseAuth;
    FirebaseUser firebaseUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_select_user_type);
        init();
    }


    public void init() {

        PermissionRequest.reqPerm();



        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("Users").child(firebaseUser.getUid());

        btn_select_parent = findViewById(R.id.btn_select_parent);
        btn_select_child = findViewById(R.id.btn_select_child);


        btn_select_child.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                databaseReference.child("handshake").child("userType").setValue("child").addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            writeKeyToDB();
                            createTrigger();
                            ActivityUtils.startActivity(SelectUserType.this, HandshakeForChild.class);
                        }
                    }
                });
            }
        });

        btn_select_parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                databaseReference.child("handshake").child("userType").setValue("parent").addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            writeKeyToDB();
                            createTrigger();
                            ActivityUtils.startActivity(SelectUserType.this, HandshakeForParent.class);
                        }
                    }
                });
            }
        });

    }

    private void createTrigger() {


        Map triggerMap = new HashMap<>();

        triggerMap.put("status","0");
        triggerMap.put("type","0");

        databaseReference.child("action").setValue(triggerMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                 /*   AlertUtils.success(ActivityUtils.getTopActivity(), "Success.",
                            "Successfull");*/
                }
            }
        });

    }

    public void writeKeyToDB() {
        databaseReference.child("handshake").child("key").setValue(RandomStringGenerator.getHandShakeKey()).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                 /*   AlertUtils.success(ActivityUtils.getTopActivity(), "Success.",
                            "Successfull");*/
                }
            }
        });

    }

}
